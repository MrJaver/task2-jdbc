package com.csu.jdbc.dao;

import com.csu.jdbc.schema.User;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class UserRepositoryImpl implements UserRepository {

    String url = "jdbc:mysql://localhost:3306/testdb";
    String username = "root";
    String password = "root";

    static {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserById(Long id) {
        return getForNameInTableAndFinderName("id", Objects.toString(id));
    }

    private User getForNameInTableAndFinderName(String name_row,String name_find){
        String lastName = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT * FROM Users WHERE "+name_row+" =?");
            statement.setString(1, name_find);
            var result = statement.executeQuery();

            return new User(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new User();
    }

    @Override
    public User getUserByFirstName(String name) {
        return getForNameInTableAndFinderName("first_name",name);
    }

    @Override
    public User getUserByLastName(String name) {
        return getForNameInTableAndFinderName("last_name",name);
    }

    @Override
    public void save(User user) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn
                    .prepareStatement("INSERT INTO Users (last_name, first_name, address, city) VALUES (?, ?, ?, ?)");
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getAddress());
            statement.setString(4, user.getCity());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
