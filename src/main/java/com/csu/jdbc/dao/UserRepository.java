package com.csu.jdbc.dao;

import com.csu.jdbc.schema.User;

public interface UserRepository {
    User getUserById(Long id);
    User getUserByFirstName(String name);
    User getUserByLastName(String name);
    void save(User user);
}
