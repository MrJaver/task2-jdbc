package com.csu.jdbc;

import com.csu.jdbc.dao.UserRepository;
import com.csu.jdbc.dao.UserRepositoryImpl;
import com.csu.jdbc.schema.User;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = new UserRepositoryImpl();

        User user = userRepository.getUserById(1L);
        User user1 = userRepository.getUserById(2L);
    }

}
