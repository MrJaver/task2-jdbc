package com.csu.jdbc.schema;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id = 0;
    private String lastName = null;
    private String firstName = null;
    private String address = null;
    private String city = null;

    public User(ResultSet result){
        try {
            while (result.next()) {
                id = result.getInt("id");
                lastName = result.getString("last_name");
                firstName = result.getString("first_name");
                address = result.getString("address");
                city = result.getString("city");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
